package Ejercicios;
//importamos el libreria swing para ingresar los datos por el teclado
import javax.swing.JOptionPane;

public class P3_Repetidos {
//hacemos uso de metodo list
	static int[] list;
// hacemos uso del metodo booleano.
	static boolean n(int num) {
		// usamos el ciclo cfor
		for (int i = 0; i < list.length; i++) {
			// usamos los condicionales
			if (list[i] == num) {
				return false;
				//retornamos false en caso de que entre al if
			}
		}
		return true;
	}
// en el main solicitamos datos
	public static void main(String[] args) {
		// solicitamos el tama�o del arreglo mediante un jFrame y hacemos creacion de variables
		int tamano = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el tama�o del arreglo: "));
		int arreglo[];
		int x = 0, num = 1;
		arreglo= new int[tamano];
		//llenamos el arreglo mediante el ciclo do while.
		while (x < tamano) {
			num = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el dato " + x + " del arreglo: "));
			arreglo[x] = num;
			x++;
		}
//igualamos la variable list del metodo al arreglo
		list = new int[arreglo.length];
// hacemos uso del ciclo for
		for (int i = 0; i < arreglo.length; i++) {

			int contador = 0;
// empezamos a recorrer el arreglo
			for (int j = 0; j < arreglo.length; j++) {
				if (arreglo[i] == arreglo[j]) {
					contador++;
					if (n(arreglo[i])) {
						list[i] = arreglo[i];
					}
				}
			}// por ultimo con un condicional mostramos cuantas veces estaria repetido el dato.
			if (list[i] != 0) {
				System.out.println("El dato " + list[i] + " del arreglo se repite: " + contador);
			}
		}
	}
}
