package Ejercicios;

//importamos el libreria swing para ingresar los datos por el teclado
import javax.swing.JOptionPane;

public class P7_Contrase�a {

	public static void main(String[] args) {
//Creamos la variable booleana para saber que nos ayude en el ciclo do while
		boolean contra = false;
		do {
			// Solicitamos la contrase�a
			String contrase�a = JOptionPane.showInputDialog("Ingrese una contrase�a:");
			// Le pasamos los parametros a la variable validador que nos ayudara a verificar
			String validador = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!\"\\\\\\\"\\\\\\\"#$%&/()=])(?=\\S+$).{10,}";
			// con el condicional if y el funcion matches verificamos que la contrase�a
			// contenga los caracteres requeridos
			if (contrase�a.matches(validador) == true) {
				// Mostramos mensaje en pantalla
				System.err.println("Contrase�a segura");
				contra = true;
			} else {
				// mostramos mensaje y ponemos la variable contra false hasta que se la
				// contrase�a sea segura para que pueda terminar
				JOptionPane.showMessageDialog(null,
						"Su contrase�a es insegura debe tener minimo para ser segura:\n�longitud mayor o igual a 10\n�Tiene al menos una letra (A-Z, a-z)\n� Tiene al menos un d�gito (0-9)\n� Tiene al menos un s�mbolo diferente a letras o d�gitos");
				contra = false;
			}
		} while (contra == false);
	}
}
