package Ejercicios;

import java.util.Arrays;
import java.util.Scanner;

public class P6_Anagramas {
	public static void main(String[] args) {
		System.out.println("Anagramas");
		
		@SuppressWarnings("resource")
		Scanner analg = new Scanner(System.in);
		// pedimos palabra a el cliente y la asignamos a String S
		System.out.println("Ingresa una palabra");
		String S = analg.nextLine();
		// pedimos posible anagrama de S a el cliente y la asignamos a String T
		System.out.println("Ingresa un Posible anagrama de " + S);
		String T = analg.nextLine();
		// comparamos para descartar que no sea la misma palabra y si lo es terminamos
		// el programa,
		if (S.equalsIgnoreCase(T)) {

			System.out.println("son la misma palabra");
			System.exit(0);

		}
		// comparamos la longitud de los dos strings y si son iguales seguimos
		if (S.length() == T.length()) {
			// tomamos los caracteres de los string y los convertimos en arrays
			char[] palabra = S.toCharArray();
			char[] Anagrama = T.toCharArray();
			// ordenamos los arrays
			Arrays.sort(palabra);
			Arrays.sort(Anagrama);
			// asignamos el resultado de ordenar el array a los strings S y T
			S = new String(palabra);
			T = new String(Anagrama);
			// comparamos si son iguales
			if (S.equalsIgnoreCase(T)) {
				// si son iguales imprime
				System.out.println("el anagrama correcto");
			}
			// si no son iguales imprime
			else {
				System.out.println("anagrama Incorrecto");
			}
		}
	}
}
