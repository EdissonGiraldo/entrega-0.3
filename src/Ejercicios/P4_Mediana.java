package Ejercicios;
//importamos el libreria scanner para ingresar los datos por el teclado
import java.util.Scanner;

public class P4_Mediana {
	static Scanner sc;
// hacemos uso de los metodos scanner y x
	static int x;

	public static void main(String[] args) {
		sc = new Scanner(System.in);
		// solicitamos el tama�o del arreglo mediante un Scanner.
		System.out.print("Escriba por favor el numero de elementos que tendra el arreglo: ");
		x = sc.nextInt();
		sc.nextLine();
//le asignamos el tama�o al arreglo
		int[] arreglo = new int[x];

		arreglo = llenarArreglo(arreglo, x);

		mediana(arreglo, x);
	}
// usamos un metodo para llenar el arreglo
	private static int[] llenarArreglo(int[] arreglo, int x) {
		int ns;
		//empezamos a llenar el arreglo mediante el ciclo for
		for (int i = 0; i < x; i++) {
			System.out.print("Escriba la cantidad de la posicion " + (i + 1) + " del arreglo: ");
			ns = sc.nextInt();
			sc.nextLine();
			arreglo[i] = ns;
		}
		System.out.println("");
		System.out.print("Arreglo: ");
		for (int i = 0; i < x - 1; i++) {
			System.out.print(arreglo[i] + ", ");
		}
		System.out.println(arreglo[x - 1]);
		return arreglo;
	}
// utlizamos otro metodo para calcular la mediana
	private static void mediana(int[] arreglo, int x) {
		int ini = (x - 1) / 2;
		// usamos los condicionales para determinar el resultados
		if ((x % 2) == 0) { // Caso par
			System.out.println(
					"Teniendo en cuenta que cantidad de elementos del arreglo es par, hay 2 valores que son su mediana y son: "
							+ arreglo[ini] + " y " + arreglo[ini + 1]);
		} else {
			System.out.println("Teniendo en cuenta que la cantidad de elementos del arreglo es impar su mediana es: "
					+ arreglo[ini]);
		}
	}
}
