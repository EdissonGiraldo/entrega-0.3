package Ejercicios;
//importamos el libreria swing para ingresar los datos por el teclado
import javax.swing.JOptionPane;

public class P1_Busqueda_arreglo {

	public static void main(String[] args) {
// solicitamos el tama�o del arreglo mediante un jFrame y hacemos creacion de variables
		int tamano = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el tama�o del arreglo:"));
		int a[];
		int i = 0, num = 1;
		boolean numero = false;
		a = new int[tamano];
		//Creamos un ciclo do while para empezar a llenar el arreglo
		while (i < tamano) {
			num = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el numero " + i + " del arreglo"));
			a[i] = num;
			i++;
		}
		// solicitamos el numero a buscar
		int n = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el numero a buscar en el arreglo:"));
// con el ciclo for buscamos el numero
		for (int j = 0; j < a.length; j++) {
			if (n == a[j]) {
				// si lo incuentra lo muestra la posicion en la que se encuentra
				System.out.println(j);
				numero = true;
			}
		}
// si no muestra el siguiente mensaje
		if (numero == false) {
			System.out.println("No se encuentra en el arreglo");
		}
	}
}
