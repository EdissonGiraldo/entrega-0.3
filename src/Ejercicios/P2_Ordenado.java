package Ejercicios;
//importamos el libreria scanner para ingresar los datos por el teclado
import java.util.Scanner;

public class P2_Ordenado {
	public static void main(String[] args) {
		@SuppressWarnings("resource")
		Scanner datos = new Scanner(System.in);
		// solicitamos el tama�o del arreglo mediante un scanner y hacemos creacion de variables
		System.out.println("Ingresa el tama�o de tu array");
		int n = datos.nextInt();
		int[] x = new int[n];
		//Creamos un ciclo for para empezar a llenar el arreglo
		for (int i = 0; i < n; i++) {
			System.out.println("Ingresa  posicion " + i + " de tu Array");
			x[i] = datos.nextInt();

		}
		//Recorremos el arreglo
		for (int j = 0; j < x.length; j++) {

			if (j + 1 < x.length) {
// usamos los condicionales para saber si esta ordenado o no.
				if (x[j] > x[j + 1]) {
					System.out.println("tu Array esta desordenado");
					break;
				}
			} else {
				System.out.println("tu Array esta en orden");
			}
		}

	}
}
